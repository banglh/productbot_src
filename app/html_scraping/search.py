# -*- coding: utf-8 -*-

import re
import urllib

from lib import network, utils, Parser

class Search(object):

    def __init__(self, settings, keyword, page = 1, **kwargs):

        self.settings = settings
        self.parser = Parser()

        self.keyword = keyword
        self.html = u''
        self.doc = None
        self.page = page
        self.products = []
        self.total = 0

    def build(self):
        self.download()
        self.parse()

    def download(self):
        # amazon search
        params = {
            '__mk_ja_JP' : 'カタカナ',
            'url' : self.settings.amazon_default_category,
            'field-keywords' : self.keyword.encode('utf-8', 'ignore')
        }

        for key in self.settings.amazon_categories.keys():
            key_arr = key.split(',')
            for k in key_arr:
                if k.strip().lower() in self.keyword.lower():
                    params['url'] = self.settings.amazon_categories[key][0]
                    break

        url = self.settings.amazon_search_url + urllib.urlencode(params)
        self.html = network.get_html(url)

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """
        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product_elements = self.doc.xpath('//li[contains(@class, "s-result-item")]')
        if len(product_elements) > 0:
            for product_element in product_elements:
                product = {
                    'name' : '',
                    'url' : '',
                    'suggestion_api' : ''
                }

                offer_url = product_element.xpath('.//a[contains(@class, "a-link-normal") and contains(@href, "offer-listing")]')
                if len(offer_url) > 0:
                    offer_url = self.parser.getAttribute(offer_url[0], 'href')
                    if offer_url and re.search(r'condition=used', offer_url) is not None:
                        continue

                product_name = product_element.xpath('.//a/h2[contains(@class, "access-title")]')
                if len(product_name) > 0:
                    product['name'] = self.parser.getText(product_name[0])

                if not product['name']:
                    continue

                product_url = product_element.xpath('.//a[contains(@class, "a-link-normal")]')
                if len(product_url) > 0:
                    product['url'] = utils.get_real_url(self.parser.getAttribute(product_url[0], 'href'))
                    product['url'] = utils.get_amazon_short_url(product['url'], self.settings.amazon_associate_tag)

                product_price = product_element.xpath('.//span[contains(@class, "a-color-price")]')
                if len(product_price) > 0:
                    product['price'] = utils.get_price(self.parser.getText(product_price[0]))

                if not product['price']:
                    continue

                product_thumbnail = product_element.xpath('.//a[contains(@class, "a-link-normal")]/img')
                if len(product_thumbnail) > 0:
                    product['thumbnail'] = utils.get_real_url(self.parser.getAttribute(product_thumbnail[0], 'src'))

                product_name = re.sub(r'\(.*\)', '', product['name'])
                product_name = re.sub(r'\s+', ' ', product_name)
                product['suggestion_api'] = self.settings.suggestion_api % (product_name)

                self.products.append(product)

        offset = (self.page - 1) * self.settings.search_per_page

        self.total = len(self.products)
        self.products = self.products[offset : offset + self.settings.search_per_page]
