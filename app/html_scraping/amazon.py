# -*- coding: utf-8 -*-

import re
import urllib

from datetime import datetime
from lib import network, utils, Parser
from common import Product

class Amazon(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.url = u''
        self.html = u''
        self.doc = None
        self.products = []

    def prepare(self, product_name):
        params = {
            '__mk_ja_JP' : 'カタカナ',
            'url' : self.settings.amazon_default_category,
        }

        names = product_name.split(' ')
        name_filter = '+'.join(names)
        name_filter = re.sub(r'\+{1,}', '+', name_filter)

        name_search = urllib.quote(name_filter.encode('utf-8', 'ignore'), safe = '+')
        self.url = self.settings.amazon_search_url + urllib.urlencode(params)
        self.url = self.url + '&field-keywords=' + name_search

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """
        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product_elements = self.doc.xpath('//li[contains(@class, "s-result-item")]')
        if len(product_elements) > 0:
            for product_element in product_elements:
                product = Product()

                offer_element = product_element.xpath('.//a[contains(@class, "a-link-normal") and contains(@href, "offer-listing")]')
                if len(offer_element) > 0:
                    offer_url = self.parser.getAttribute(offer_element[0], 'href')
                    if not offer_url or (re.search(r'condition=new', offer_url) is None and re.search(r'condition=used', offer_url) is not None):
                        continue

                    total_product = offer_element[0].xpath(u'./span[contains(text(), "出品")]')
                    if len(total_product) > 0:
                        product.total = self.parser.getText(total_product[0])
                        product.total = re.sub(ur'[^0-9]', '', product.total).strip()

                product_name = product_element.xpath('.//a/h2[contains(@class, "access-title")]')
                if len(product_name) > 0:
                    product.name = self.parser.getText(product_name[0])

                if not product.name:
                    continue

                product_url = product_element.xpath('.//a[contains(@class, "a-link-normal")]')
                if len(product_url) > 0:
                    product.url = utils.get_real_url(self.parser.getAttribute(product_url[0], 'href'))
                    product.url = utils.get_amazon_short_url(product.url, self.settings.amazon_associate_tag)

                if not product.url:
                    continue

                product_price = product_element.xpath('.//span[contains(@class, "a-color-price")]')
                if len(product_price) > 0:
                    product.price = utils.get_price(self.parser.getText(product_price[0]))

                if not product.price:
                    continue
                product.price = float(product.price)

                product_thumbnail = product_element.xpath('.//a[contains(@class, "a-link-normal")]/img')
                if len(product_thumbnail) > 0:
                    product.thumbnail = utils.get_real_url(self.parser.getAttribute(product_thumbnail[0], 'src'))

                product_delivery = product_element.xpath(u'.//span[contains(text(), "中にお届け")]/span')
                if len(product_delivery) > 0:
                    delivery = self.parser.getText(product_delivery[0])
                    search_date = re.search(u'\d{4}/\d{1,2}/\d{1,2}', delivery)
                    if search_date is not None:
                        delivery_date = search_date.group(0)
                        delivery_days = (datetime.strptime(delivery_date, "%Y/%m/%d").date() - datetime.now().date()).days
                        product.delivery_date_min = delivery_days
                        product.delivery_date_max = delivery_days

                product.type = 'amazon'

                self.products.append(product)

                if len(self.products) >= self.settings.product_per_site:
                    break
