# -*- coding: utf-8 -*-

import re
import json
import urllib

from lib import network, utils, Parser
from common import Product

class Rakuten(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.url = u''
        self.html = u''
        self.doc = None
        self.products = []

    def prepare(self, product_name):
        names = product_name.split(' ')
        name_filter = '+'.join(names)
        name_filter = re.sub(r'\+{1,}', '+', name_filter)

        name_search = urllib.quote(name_filter.encode('utf-8', 'ignore'), safe = '+')
        self.url = self.settings.rakuten_search_url % name_search

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """
        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product_elements = self.doc.xpath('//div[contains(@class, "rsrSResultSect") and not(contains(@class, "rsrSResultSectPr"))]')
        if len(product_elements) > 0:
            for product_element in product_elements:
                product = Product()

                product_name = product_element.xpath('./div[contains(@class, "rsrSResultItemTxt")]/h2/a')
                if len(product_name) > 0:
                    product.name = self.parser.getText(product_name[0])

                if not product.name:
                    continue

                if utils.is_second_hand_product(product.name):
                    continue

                if len(product_name) > 0:
                    product.url = utils.get_real_url(self.parser.getAttribute(product_name[0], 'href'))

                if not product.url:
                    continue

                product_price = product_element.xpath('./div[contains(@class, "rsrSResultItemInfo")]/p[@class="price"]/a')
                if len(product_price) > 0:
                    product.price = utils.get_price(self.parser.getText(product_price[0]))

                if not product.price:
                    continue
                product.price = float(product.price)

                product_thumbnail = product_element.xpath('./div[contains(@class, "rsrSResultPhoto")]/a/img')
                if len(product_thumbnail) > 0:
                    product.thumbnail = utils.get_real_url(self.parser.getAttribute(product_thumbnail[0], 'src'))

                asuraku = product_element.xpath('./div[contains(@class, "rsrSResultItemInfo")]//p[@class="iconAsuraku"]')
                if len(asuraku) > 0:
                    product.delivery_date_min = 1
                    product.delivery_date_max = 1

                product_delivery = product_element.xpath('./div[contains(@class, "rsrSResultItemInfo")]/div[@class="deliveryText"]')
                if len(product_delivery) > 0:
                    delivery_date = utils.get_delivery_date(self.parser.getText(product_delivery[0]))
                    if delivery_date:
                        if delivery_date[0] is not None:
                            product.delivery_date_min = delivery_date[0]

                        if delivery_date[1] is not None:
                            product.delivery_date_max = delivery_date[1]

                product.type = 'rakuten'

                self.products.append(product)

                if len(self.products) >= self.settings.product_per_site:
                    break
