# -*- coding: utf-8 -*-

import re
import urllib

from datetime import datetime
from lib import network, utils, Parser

class AmazonProduct(object):

    def __init__(self, settings, asin, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.asin = asin
        self.url = u''
        self.html = u''
        self.doc = None
        self.product = None

    def build(self):
        self.prepare()
        self.download()
        self.parse()

    def prepare(self):
        self.url = self.settings.amazon_product_url % self.asin

    def download(self):
        """Downloads html of source
        """
        self.html = network.get_html(self.url)

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """
        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product = { \
            'ASIN' : '',
            'Title' : '',
            'LargeImageUrl' : '',
            'SmallImageUrl' : '',
            'DetailPageUrl' : '',
            'EditoriealReviews' : '',
            'NewPrice' : '',
            'UsedPrice' : '',
            'TotalNew' : '',
            'TotalUsed' : '',
            'Brand' : '',
            'Manufacturer' : '',
            'Model' : '',
            'Feature' : '',
            'Reviews' : '',
            'Category' : '',
            'Availability' : '',
            'DeliveryDateMin' : '',
            'DeliveryDateMax' : '',
        }
        product['ASIN'] = self.asin

        title_element = self.doc.xpath('//span[@id="productTitle"]')
        if len(title_element) > 0:
            product['Title'] = self.parser.getText(title_element[0])

        if not product['Title']:
            return

        large_image_element = self.doc.xpath('//div[@id="imgTagWrapperId"]/img')
        if len(large_image_element) > 0:
            data_image = self.parser.getAttribute(large_image_element[0], 'data-a-dynamic-image')
            data_image = re.sub(r'{|}|\"|:\[\d+,\d+\]', '', data_image)
            product['LargeImageUrl'] = data_image.split(',')[0]

        small_image_element = self.doc.xpath('//div[@id="altImages"]//span[@class="a-list-item"]//img')
        if len(small_image_element) > 0:
            product['SmallImageUrl'] = self.parser.getAttribute(small_image_element[0], 'src')

        product['DetailPageUrl'] = self.url

        editorieal_review_element = self.doc.xpath('//div[@id="productDescription"]/p')
        if len(editorieal_review_element) > 0:
            product['EditoriealReviews'] = self.parser.getText(editorieal_review_element[0])

        new_price_element = self.doc.xpath('//a[contains(@href, "condition=new")]/following-sibling::span[1]')
        if len(new_price_element) > 0:
           product['NewPrice'] = utils.get_price(self.parser.getText(new_price_element[0]))

        used_price_element = self.doc.xpath('//a[contains(@href, "condition=used")]/following-sibling::span[1]')
        if len(used_price_element) > 0:
           product['UsedPrice'] = utils.get_price(self.parser.getText(used_price_element[0]))

        total_new_element = self.doc.xpath('//a[contains(@href, "condition=new")]')
        if len(total_new_element) > 0:
            total_new = self.parser.getText(total_new_element[0])
            search = re.search('(\d+)', total_new)
            if search is not None:
                product['TotalNew'] = search.groups()[0]

        total_used_element = self.doc.xpath('//a[contains(@href, "condition=used")]')
        if len(total_used_element) > 0:
            total_used = self.parser.getText(total_used_element[0])
            search = re.search('(\d+)', total_used)
            if search is not None:
                product['TotalUsed'] = search.groups()[0]

        brand_element = self.doc.xpath('//a[@id="brand"]')
        if len(brand_element) > 0:
            product['Brand'] = self.parser.getText(brand_element[0])

        product['Manufacturer'] = product['Brand']

        model_element = self.doc.xpath('//tr[@class="item-model-number"]/td[@class="value"]')
        if len(model_element) > 0:
            product['Model'] = self.parser.getText(model_element[0])

        feature_elements = self.doc.xpath('//div[@id="feature-bullets"]/ul/li')
        feature = []
        for element in feature_elements:
            feature.append(self.parser.getText(element))

        product['Feature'] = ' '.join(feature)

        product['Reviews'] = 'https://www.amazon.co.jp/product-reviews/' + self.asin

        breadcrumb_element = self.doc.xpath('//div[@id="wayfinding-breadcrumbs_feature_div"]/ul/li/span/a')
        if len(breadcrumb_element) > 0:
            product['Category'] = self.parser.getText(breadcrumb_element[-1])

        availability_element = self.doc.xpath('//div[@id="availability"]/span')
        if len(availability_element) > 0:
            product['Availability'] = self.parser.getText(availability_element[0])
            delivery_date = utils.get_delivery_date(product['Availability'])
            if delivery_date:
                product['DeliveryDateMin'] = delivery_date[0]
                product['DeliveryDateMax'] = delivery_date[1]

        self.product = product

        for key in self.product.keys():
            if not self.product[key]:
                self.product.pop(key, None)
                continue

            if 'EAN' not in key (isinstance(self.product[key], str) or isinstance(self.product[key], unicode)) \
                and re.search(r'^\d+$', self.product[key]) is not None:
                self.product[key] = float(self.product[key])
