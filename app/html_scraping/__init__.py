# -*- coding: utf-8 -*-
from search import Search as HtmlSearch
from amazon import Amazon as HtmlAmazon
from kakaku import Kakaku as HtmlKakaku
from rakuten import Rakuten as HtmlRakuten
from mercari import Mercari as HtmlMercari
from amazon_product import AmazonProduct as HtmlAmazonProduct
