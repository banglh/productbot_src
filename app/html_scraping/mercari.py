# -*- coding: utf-8 -*-

import re
import urllib

from lib import network, utils, Parser
from common import Product

class Mercari(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.url = u''
        self.html = u''
        self.doc = None
        self.products = []
        self.detail_urls = []
        self.detail_htmls = []

    def prepare(self, product_name):
        names = product_name.split(' ')
        name_filter = '+'.join(names)
        name_filter = re.sub(r'\+{1,}', '+', name_filter)
        name_search = urllib.quote(name_filter.encode('utf-8', 'ignore'), safe = '+')
        self.url = self.settings.mercari_search_url % name_search

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """

        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product_not_found = self.doc.xpath('//div[@class="l-content"]/section/section[contains(@class, "items-box-container")]/p[@class="search-result-description"]')
        if len(product_not_found) > 0:
            return

        product_elements = self.doc.xpath('//div[contains(@class, "items-box-content")]/section[@class="items-box"]')
        if len(product_elements) > 0:
            for product_element in product_elements:
                product = Product()

                sold_element = product_element.xpath('./a/figure[@class="items-box-photo"]/figcaption/div[@class="item-sold-out-badge"]/div')
                if len(sold_element) > 0:
                    continue

                url_element = product_element.xpath('./a')
                if len(url_element) > 0:
                    product.url = utils.get_real_url(self.parser.getAttribute(url_element[0], 'href'))

                if not product.url:
                    continue

                name_element = product_element.xpath('./a/div[@class="items-box-body"]/h3[contains(@class, "items-box-name")]')
                if len(name_element) > 0:
                    product.name = self.parser.getText(name_element[0])

                if not product.name:
                    continue

                if utils.is_second_hand_product(product.name):
                    continue

                price_element = product_element.xpath('./a/div[@class="items-box-body"]//div[contains(@class, "items-box-price")]')
                if len(price_element) > 0:
                    product.price = utils.get_price(self.parser.getText(price_element[0]))

                if not product.price:
                    continue
                product.price = float(product.price)

                product_thumbnail = product_element.xpath('./a/figure[@class="items-box-photo"]/img')
                if len(product_thumbnail) > 0:
                    product.thumbnail = self.parser.getAttribute(product_thumbnail[0], 'data-src')

                product.type = 'mercari'
                self.products.append(product)
                if len(self.products) >= 3:
                    break

        self.detail_urls = [product.url for product in self.products]

    def parse_detail(self):
        index = -1
        for html in self.detail_htmls:
            index = index + 1
            doc = self.parser.fromstring(html)

            if doc is None:
                continue

            # manufacture_element = doc.xpath('//table[@class="item-detail-table"]/tr[3]/td')
            # if len(manufacture_element) > 0:
            #     self.products[index].manufacture = self.parser.getText(manufacture_element[0])

            delivery_element = doc.xpath('//table[@class="item-detail-table"]/tr[last()]/td')
            if len(delivery_element) > 0:
                delivery_date = utils.get_delivery_date(self.parser.getText(delivery_element[0]))
                if delivery_date:
                    self.products[index].delivery_date_min = delivery_date[0]
                    self.products[index].delivery_date_max = delivery_date[1]
