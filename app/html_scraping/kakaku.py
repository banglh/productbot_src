# -*- coding: utf-8 -*-

import re
import urllib

from lib import network, utils, Parser
from common import Product

class Kakaku(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.url = u''
        self.html = u''
        self.doc = None
        self.products = []
        self.detail_urls = []
        self.detail_htmls = []

    def prepare(self, product_name):
        names = product_name.split(' ')
        name_filter = '+'.join(names)
        name_filter = re.sub(r'\+{1,}', '+', name_filter)
        name_search = urllib.quote(name_filter.encode('shift-jis', 'ignore'), safe = '+')
        self.url = self.settings.kakaku_search_url % name_search

    def parse(self):
        """Sets the lxml root, also sets lxml roots of all children link """

        self.doc = self.parser.fromstring(self.html)
        if self.doc is None:
            return

        product_elements = self.doc.xpath('//div[contains(@class, "itemtblList")]/div[contains(@class, "item")]')

        if len(product_elements) > 0:
            for product_element in product_elements:
                product = Product()

                product_url = product_element.xpath('.//div[@class="itemphoto"]/a')
                if len(product_url) > 0:
                    product.url = utils.get_real_url(self.parser.getAttribute(product_url[0], 'href'))
                    product.url = utils.get_amazon_short_url(product.url, self.settings.amazon_associate_tag)

                    if not product.url:
                        continue

                    product_thumbnail = product_element.xpath('.//div[@class="itemphoto"]/a/img')
                    if len(product_thumbnail) > 0:
                        product.thumbnail = self.parser.getAttribute(product_thumbnail[0], 'data-original')

                    product_type = self.parser.getAttribute(product_url[0], 'class')

                    if 'selfLink' not in product_type:
                        product_name = product_element.xpath('.//div[@class="itemInfo"]/p[@class="itemnameN"]')
                        if len(product_name) > 0:
                            product.name = self.parser.getText(product_name[0])

                        if not product.name:
                            continue

                        if utils.is_second_hand_product(product.name):
                            continue

                        product_price = product_element.xpath('.//p[@class="price"]/span[@class="yen"]')
                        if len(product_price) > 0:
                            product.price = utils.get_price(self.parser.getText(product_price[0]))

                        if not product.price:
                            continue
                        product.price = float(product.price)

                        product.type = 'kakaku'

                        self.products.append(product)

                    else:
                        self.detail_urls.append(product.url)

                    if len(self.products) + len(self.detail_urls) >= 3:
                        break

    def parse_detail(self):
        for html in self.detail_htmls:
            doc = self.parser.fromstring(html)

            if doc is None:
                continue

            product = Product()

            thumbnail_element = doc.xpath('//div[@id="imgBox"]//img')
            if len(thumbnail_element) > 0:
                product.thumbnail = self.parser.getAttribute(thumbnail_element[0], 'src')

            name_element = doc.xpath('//div[@id="main"]//div[@id="itmArea"]//div[@id="titleBox"]//*[@itemprop="name"]')
            if len(name_element) > 0:
                product.name = self.parser.getText(name_element[0])
                if utils.is_second_hand_product(product.name):
                    continue

            manufacture_element = doc.xpath('//li[@class="makerLabel"]/a')
            if len(manufacture_element) > 0:
                product.manufacture = self.parser.getText(manufacture_element[0])

            rows = doc.xpath('//div[@id="main"]//div[@id="itemv"]//div[@id="tabContents"]//table[@class="tblBorderGray"]//tr')
            for row in rows:
                columns = row.xpath('./td')
                if len(columns) < 8:
                    continue

                product_price = columns[1].xpath('./p[@class="fontPrice wordwrapPrice"]')
                if len(product_price) > 0:
                    product.price = utils.get_price(self.parser.getText(product_price[0]))

                if not product.price:
                    continue
                product.price = float(product.price)

                availability = self.parser.getText(columns[3])
                if availability and re.search(ur'有', availability) is not None:
                    product.availability = 1

                delivery_date = utils.get_delivery_date(availability)
                if delivery_date:
                    product.delivery_date_min = delivery_date[0]
                    product.delivery_date_max = delivery_date[1]

                shop_url = columns[7].xpath('./a')
                if len(shop_url) > 0:
                    product.url = utils.get_real_url(self.parser.getAttribute(shop_url[0], 'href'))
                    product.url = utils.get_amazon_short_url(product.url, self.settings.amazon_associate_tag)

                product.type = 'kakaku'
                self.products.append(product)
                break
