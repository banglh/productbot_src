# -*- coding: utf-8 -*-

import time
import re
from lib import network
from aws import Aws

class AmazonProduct(object):

    def __init__(self, settings, asin, **kwargs):
        self.settings = settings
        self.asin = asin
        self.url = u''
        self.html = u''
        self.product = None

    def build(self):
        for i in range(self.settings.retry_time):
            self.prepare()
            self.download()

            if self.html:
                self.parse()
                break

    def prepare(self):
        params = {
            'Operation': 'ItemLookup',
            'IdType': 'ASIN',
            'ItemId': self.asin,
            'ResponseGroup' : 'Large,Small'
        }

        self.url = Aws(self.settings).get_signed_url(params)

    def download(self):
        """Downloads html of source
        """
        self.html = network.get_html(self.url)

    def parse(self):
        if not self.html:
            return

        products = Aws(self.settings).transform_full_item_lookup(self.html)
        if len(products) > 0:
            self.product = products[0]
            for key in self.product.keys():
                if not self.product[key]:
                    self.product.pop(key, None)
                    continue

                if 'EAN' not in key and (isinstance(self.product[key], str) or isinstance(self.product[key], unicode)) \
                    and re.search(r'^\d+$', self.product[key]) is not None:
                    self.product[key] = float(self.product[key])
