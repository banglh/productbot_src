# -*- coding: utf-8 -*-

import hmac
import urllib
import time
import base64
import copy
import re

from hashlib import sha256
from lxml import etree
from random import randint

from common import Product
from lib import utils

class Aws():

    def __init__(self, settings):
        self.settings = settings
        index = randint(0, len(self.settings.aws_accounts) - 1)
        self.accessKeyId = self.settings.aws_accounts[index][0]
        self.secretAccessKey = self.settings.aws_accounts[index][1]
        self.associateTag = self.settings.aws_accounts[index][2]

    def newhmac(self):
        return hmac.new(self.secretAccessKey, digestmod = sha256)

    def get_signed_url(self, params):
        hmac = self.newhmac()
        action = 'GET'
        server = "webservices.amazon.co.jp"
        path = "/onca/xml"

        params['Version'] = '2013-08-01'
        params['AWSAccessKeyId'] = self.accessKeyId
        params['AssociateTag'] = self.associateTag
        params['Service'] = 'AWSECommerceService'
        params['Timestamp'] = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

        key_values = [(urllib.quote(k), urllib.quote(v)) for k, v in params.items()]
        key_values.sort()

        paramstring = '&'.join(['%s=%s' % (k, v) for k, v in key_values])

        urlstring = "http://" + server + path + "?" + \
            ('&'.join(['%s=%s' % (k, v) for k, v in key_values]))

        paramstring = re.sub(r'\/', '%2F', paramstring)
        hmac.update(action + "\n" + server + "\n" + path + "\n" + paramstring)

        urlstring = urlstring + "&Signature=" + \
            urllib.quote(base64.encodestring(hmac.digest()).strip())

        return urlstring

    def get_value(self, node, xpath):
        ns = {'n':self.settings.aws_api_namespace}

        attrValue = node.xpath(xpath, namespaces = ns)
        if len(attrValue) > 0:
            return attrValue[0].strip()

        return ''

    def get_values(self, node, xpath):
        ns = {'n':self.settings.aws_api_namespace}

        attrValue = node.xpath(xpath, namespaces = ns)
        if len(attrValue) > 0:
            values = ' '.join([value.strip() for value in attrValue])
            return values.strip()

        return ''

    def get_node(self, node, xpath):
        ns = {'n':self.settings.aws_api_namespace}

        nodes = node.xpath(xpath, namespaces = ns)
        if len(nodes) > 0:
            return nodes[0]

        return None

    def get_nodes(self, node, xpath):
        ns = {'n':self.settings.aws_api_namespace}

        return node.xpath(xpath, namespaces = ns)

    def transform_item_search(self, content):
        products = []

        doc = etree.fromstring(content)

        items = self.get_nodes(doc, '//n:ItemSearchResponse//n:Item')
        for item in items:
            product = {}

            product['asin'] = self.get_value(item, 'n:ASIN/text()')
            product['name'] = self.get_value(item, 'n:ItemAttributes/n:Title/text()')

            product['url'] = utils.get_amazon_short_url(self.get_value(item, 'n:DetailPageURL/text()'),
                                    self.settings.amazon_associate_tag)

#             product_name = re.sub(r'\(.*\)', '', product['name'])
#             product_name = re.sub(r'\s+', ' ', product_name)
#             product['suggestion_api'] = self.settings.suggestion_api % (product_name.strip())
            product['suggestion_api'] = self.settings.suggestion_api % (product['name'].strip())
            product['thumbnail'] = ''

            products.append(product)

        return products

    def transform_item_lookup_for_search(self, content):
        products = []

        doc = etree.fromstring(content)

        items = self.get_nodes(doc, '//n:ItemLookupResponse//n:Items//n:Item')
        for item in items:
            product = {}

            price = self.get_value(item, 'n:OfferSummary/n:LowestNewPrice/n:FormattedPrice/text()')
            price = utils.get_price(price)
            if not price:
                continue

            product['name'] = self.get_value(item, 'n:ItemAttributes/n:Title/text()')
            product['url'] = utils.get_amazon_short_url(self.get_value(item, 'n:DetailPageURL/text()'),
                                                        self.settings.amazon_associate_tag)
            product['thumbnail'] = self.get_value(item, 'n:MediumImage/n:URL/text()')

            product['suggestion_api'] = self.settings.suggestion_api % (product['name'].strip())

            products.append(product)

        return products

    def transform_item_lookup(self, content):
        products = []

        doc = etree.fromstring(content)

        items = self.get_nodes(doc, '//n:ItemLookupResponse//n:Items//n:Item')
        for item in items:
            product = Product()

            product.name = self.get_value(item, 'n:ItemAttributes/n:Title/text()')
            product.url = utils.get_amazon_short_url(self.get_value(item, 'n:DetailPageURL/text()'),
                                                     self.settings.amazon_associate_tag)
            price = self.get_value(item, 'n:OfferSummary/n:LowestNewPrice/n:FormattedPrice/text()')

            price = utils.get_price(price)
            if not price:
                continue

            product.price = float(price)

            product.manufacture = self.get_value(item, 'n:ItemAttributes/n:Manufacturer/text()')

            availability = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:Availability/text()')

            if availability and re.search(ur'在庫あり', availability) is not None:
                product.availability = 1

            delivery_date = utils.get_delivery_date(availability)
            if delivery_date:
                product.delivery_date_min = delivery_date[0]
                product.delivery_date_max = delivery_date[1]

            product.thumbnail = self.get_value(item, 'n:MediumImage/n:URL/text()')
            product.total = self.get_value(item, 'n:OfferSummary/n:TotalNew/text()')
            product.type = 'amazon'

            products.append(product)

        return products

    def transform_full_item_lookup(self, content):
        doc = etree.fromstring(content)
        items = self.get_nodes(doc, '//n:ItemLookupResponse//n:Items//n:Item')

        products = []

        for item in items:
            product = { \
                'ASIN' : '',
                'Title' : '',
                'LargeImageUrl' : '',
                'SmallImageUrl' : '',
                'DetailPageUrl' : '',
                'EditoriealReviews' : '',
                'NewPrice' : '',
                'UsedPrice' : '',
                'TotalNew' : '',
                'TotalUsed' : '',
                'TotalCollectible' : '',
                'TotalRefurbished' : '',
                'Binding' : '',
                'Brand' : '',
                'CatalogNumberList' : '',
                'EAN' : '',
                'EANList' : '',
                'Feature' : '',
                'IsAdultProduct' : '',
                'ItemDimensions_height' : '',
                'ItemDimensions_length' : '',
                'ItemDimensions_weight' : '',
                'ItemDimensions_width' : '',
                'Label' : '',
                'Languages' : '',
                'Manufacturer' : '',
                'Model' : '',
                'MPN' : '',
                'NumberOfItems' : '',
                'PackageDimensions_height' : '',
                'PackageDimensions_length' : '',
                'PackageDimensions_weight' : '',
                'PackageDimensions_width' : '',
                'PackageQuantity' : '',
                'PartNumber' : '',
                'ProductGroup' : '',
                'ProductTypeName' : '',
                'Publisher' : '',
                'Studio' : '',
                'UPC' : '',
                'UPCList' : '',
                'Reviews' : '',
                'SalesRank' : '',
                'Availability' : '',
                'AvailabilityType' : '',
                'Availability_MinimumHours' : '',
                'Availability_MaximumHours' : '',
                'DeliveryDateMin' : '',
                'DeliveryDateMax' : '',
                'Category' : '',
            }

            product['ASIN'] = self.get_value(item, 'n:ASIN/text()')
            product['LargeImageUrl'] = self.get_value(item, 'n:LargeImage/n:URL/text()')
            product['SmallImageUrl'] = self.get_value(item, 'n:SmallImage/n:URL/text()')
            product['DetailPageUrl'] = utils.get_amazon_short_url(self.get_value(item, 'n:DetailPageURL/text()'),
                                                                  self.settings.amazon_associate_tag)
            product['ProductGroup'] = self.get_value(item, 'n:ItemAttributes/n:ProductGroup/text()')
            product['SalesRank'] = self.get_value(item, 'n:SalesRank/text()')
            product['EditoriealReviews'] = self.get_values(item, 'n:EditorialReviews/n:EditorialReview/n:Content/text()')

            offerSummaryElement = self.get_node(item, 'n:OfferSummary')
            if offerSummaryElement is not None:
                product['NewPrice'] = self.get_value(offerSummaryElement, 'n:LowestNewPrice/n:FormattedPrice/text()')
                product['NewPrice'] = utils.get_price(product['NewPrice'])

                product['UsedPrice'] = self.get_value(offerSummaryElement, 'n:LowestUsedPrice/n:FormattedPrice/text()')
                product['UsedPrice'] = utils.get_price(product['UsedPrice'])

                product['TotalNew'] = self.get_value(offerSummaryElement, 'n:TotalNew/text()')
                product['TotalUsed'] = self.get_value(offerSummaryElement, 'n:TotalUsed/text()')
                product['TotalCollectible'] = self.get_value(offerSummaryElement, 'n:TotalCollectible/text()')
                product['TotalRefurbished'] = self.get_value(offerSummaryElement, 'n:TotalRefurbished/text()')

            product['Reviews'] = self.get_value(item, 'n:CustomerReviews/n:IFrameURL/text()')

            # shipping info
            availability = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:Availability/text()')
            if re.search(u'発送', availability) is not None:
                product['Availability'] = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:Availability/text()')
                product['AvailabilityType'] = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:AvailabilityAttributes/n:AvailabilityType/text()')
                product['Availability_MinimumHours'] = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:AvailabilityAttributes/n:MinimumHours/text()')
                product['Availability_MaximumHours'] = self.get_value(item, 'n:Offers/n:Offer/n:OfferListing/n:AvailabilityAttributes/n:MaximumHours/text()')

                deliveryDate = utils.get_delivery_date(product['Availability'])
                if deliveryDate:
                    product['DeliveryDateMin'] = deliveryDate[0]
                    product['DeliveryDateMax'] = deliveryDate[1]

            attElement = self.get_node(item, 'n:ItemAttributes')
            if attElement is not None:
                product['Binding'] = self.get_value(attElement, 'n:Binding/text()')
                product['Brand'] = self.get_value(attElement, 'n:Brand/text()')

                product['CatalogNumberList'] = self.get_values(attElement, 'n:CatalogNumberListElement/text()')
                product['EAN'] = self.get_value(attElement, 'n:EAN/text()')
                product['EANList'] = self.get_values(attElement, 'n:EANList/n:EANListElement/text()')
                product['Feature'] = self.get_values(attElement, 'n:Feature/text()')
                product['IsAdultProduct'] = self.get_value(attElement, 'n:IsAdultProduct/text()')

                dimensionElement = self.get_node(attElement, 'n:ItemDimensions')
                if dimensionElement is not None:
                    product['ItemDimensions_height'] = self.get_value(dimensionElement, 'n:Height/text()')
                    product['ItemDimensions_length'] = self.get_value(dimensionElement, 'n:Length/text()')
                    product['ItemDimensions_weight'] = self.get_value(dimensionElement, 'n:Weight/text()')
                    product['ItemDimensions_width'] = self.get_value(dimensionElement, 'n:Width/text()')

                product['Label'] = self.get_value(attElement, 'n:Label/text()')
                product['Manufacturer'] = self.get_value(attElement, 'n:Manufacturer/text()')
                product['Model'] = self.get_value(attElement, 'n:Model/text()')
                product['MPN'] = self.get_value(attElement, 'n:MPN/text()')
                product['NumberOfItems'] = self.get_value(attElement, 'n:NumberOfItems/text()')

                pdimensionElement = self.get_node(attElement, 'n:PackageDimensions')
                if pdimensionElement is not None:
                    product['PackageDimensions_height'] = self.get_value(pdimensionElement, 'n:Height/text()')
                    product['PackageDimensions_length'] = self.get_value(pdimensionElement, 'n:Length/text()')
                    product['PackageDimensions_weight'] = self.get_value(pdimensionElement, 'n:Weight/text()')
                    product['PackageDimensions_width'] = self.get_value(pdimensionElement, 'n:Width/text()')

                product['PackageQuantity'] = self.get_value(attElement, 'n:PackageQuantity/text()')
                product['PartNumber'] = self.get_value(attElement, 'n:PartNumber/text()')
                product['ProductGroup'] = self.get_value(attElement, 'n:ProductGroup/text()')
                product['ProductTypeName'] = self.get_value(attElement, 'n:ProductTypeName/text()')
                product['Publisher'] = self.get_value(attElement, 'n:Publisher/text()')
                product['Studio'] = self.get_value(attElement, 'n:Studio/text()')
                product['Title'] = self.get_value(attElement, 'n:Title/text()')
                product['UPC'] = self.get_value(attElement, 'n:UPC/text()')
                product['ReleaseDate'] = self.get_value(attElement, 'n:ReleaseDate/text()')

                product['UPCList'] = self.get_values(attElement, 'n:UPCList/n:UPCListElement/text()')
                product['Languages'] = self.get_value(attElement, 'n:Languages/n:Language/n:Name/text()')

            cats = set()
            catElements = self.get_nodes(item, 'n:BrowseNodes/n:BrowseNode')
            for element in catElements:
                cats.add(self.get_value(element, 'n:Name/text()'))

                tmpElement = copy.deepcopy(element)
                for i in range(0, 10):
                    parentElement = self.get_node(tmpElement, 'n:Ancestors/n:BrowseNode')

                    if parentElement is None:
                        break

                    cats.add(self.get_value(parentElement, 'n:Name/text()'))
                    tmpElement = parentElement

            product['Category'] = ' '.join([cat for cat in cats])

            products.append(product)

        return products
