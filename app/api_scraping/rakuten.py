# -*- coding: utf-8 -*-

import re
import json
import urllib

from random import randint
from lib import network, utils, Parser
from common import Product

class Rakuten(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.parser = Parser()

        self.url = u''
        self.html = u''
        self.products = []

    def prepare(self, product_name):
        product_name = product_name[0:128]  # max length rakuten keyword is 128
        name_search = urllib.quote(product_name.encode('utf-8', 'ignore'))
        app_index = randint(0, len(self.settings.rakuten_app_ids) - 1)
        self.url = self.settings.rakuten_product_api % (name_search, self.settings.rakuten_app_ids[app_index])

    def parse(self):
        if not self.html:
            return

        response = json.loads(self.html)

        if 'Items' not in response:
            return

        items = response['Items']
        for item in items:
            if 'Item' not in item:
                continue

            data = item['Item']

            product = Product()
            product.name = self.getValue(data, 'itemName')

            if utils.is_second_hand_product(product.name):
                continue

            product.price = self.getValue(data, 'itemPrice')

            if not product.price:
                continue

            product.url = utils.get_real_url(self.getValue(data, 'itemUrl'))
            product.thumbnail = self.getValue(data, 'mediumImageUrls_imageUrl')
            product.availability = self.getValue(data, 'availability')
            product.type = 'rakuten'

            asuraku_flag = self.getValue(data, 'asurakuFlag')
            if asuraku_flag and asuraku_flag == 1:
                product.delivery_date_min = 1
                product.delivery_date_max = 1

            self.products.append(product)

    def getValue(self, data, attribute):
        value = ''
        attrs = attribute.split('_')

        for attr in attrs:
            if attr not in data:
                return ''

            if isinstance(data[attr], list) and data[attr]:
                value = data[attr][0]
            else:
                value = data[attr]

            data = value

        return value
