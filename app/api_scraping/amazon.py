# -*- coding: utf-8 -*-

from lib import network, utils, Parser
from common import Product
from aws import Aws

class Amazon(object):

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.url = u''
        self.html = u''
        self.products = []
        self.detail_urls = []
        self.detail_htmls = []

    def prepare(self, product_name):
        params = {
            'Operation': 'ItemSearch',
            'Keywords': product_name.encode('utf-8', 'ignore'),
            'SearchIndex' : 'All'
        }

        self.url = Aws(self.settings).get_signed_url(params)

    def parse(self):
        if not self.html:
            return

        products = Aws(self.settings).transform_item_search(self.html)

        product_codes = [product['asin'] for product in products]
        if len(product_codes) == 0:
            return

        params = {
            'Operation': 'ItemLookup',
            'IdType': 'ASIN',
            'ItemId': ','.join(product_codes),
            'ResponseGroup' : 'Large,Small'
        }

        self.detail_urls.append(Aws(self.settings).get_signed_url(params))

    def parse_detail(self):
        for html in self.detail_htmls:
            if not html:
                continue

            self.products = self.products + Aws(self.settings).transform_item_lookup(html)
