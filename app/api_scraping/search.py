# -*- coding: utf-8 -*-

import re

from lib import network, utils, Parser
from aws import Aws

class Search(object):

    def __init__(self, settings, keyword, page = 1, ** kwargs):

        self.settings = settings
        self.parser = Parser()

        self.keyword = keyword
        self.html = u''
        self.doc = None
        self.page = page
        self.search_index = 'All'
        self.params = {}
        self.products = []
        self.has_data = False
        self.total = 0

    def build(self):
        self.prepare()

        for i in range(self.settings.retry_time):
            self.download()
            self.parse()

            if self.html:
                break

        if len(self.products) > 0:
            self.has_data = True

        offset = (self.page - 1) * self.settings.search_per_page
        self.products = self.products[offset : offset + self.settings.search_per_page]

    def prepare(self):
        for key in self.settings.amazon_categories.keys():
            key_arr = key.split(',')
            for k in key_arr:
                if k.strip().lower() in self.keyword.lower():
                    self.search_index = self.settings.amazon_categories[key][1]
                    break

        # amazon item search
        self.params = {
            'Operation': 'ItemSearch',
            'Keywords': self.keyword.encode('utf-8', 'ignore'),
            'SearchIndex' : self.search_index,
            'Condition' : 'New'
        }

    def download(self):
        url = Aws(self.settings).get_signed_url(self.params)
        self.html = network.get_html(utils.get_unicode(url))

    def parse(self):
        if not self.html:
            return

        products = Aws(self.settings).transform_item_search(self.html)

#         if self.search_index == 'All':
#             self.products = products
#             self.parse_new_product()
#         else:
#             for product in products:
#                 product.pop('asin', None)
#                 self.products.append(product)
        self.products = products
        self.parse_new_product()

    def parse_new_product(self):
        products = []
        product_codes = [product['asin'] for product in self.products]

        if len(product_codes) == 0:
            return products

        params = {
            'Operation': 'ItemLookup',
            'IdType': 'ASIN',
            'ItemId': ','.join(product_codes),
            'ResponseGroup' : 'Large,Small'
        }

        retry_flg = True
        for i in range(self.settings.retry_time):
            url = Aws(self.settings).get_signed_url(params)

            requests = network.multithread_request([url])

            for index, req in enumerate(requests):
                html = network.get_html(req.url, response = req.resp)

                if not html:
                    continue

                products = products + Aws(self.settings).transform_item_lookup_for_search(html)
                retry_flg = False

            if not retry_flg:
                break

        self.products = products
        self.total = len(self.products)

