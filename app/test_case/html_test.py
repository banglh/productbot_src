# -*- coding: utf-8 -*-

import os
import sys
sys.path.append(os.path.realpath('..'))

import re
import logging
import settings

from datetime import datetime
from lib.sendmail import SendMail
from html_scraping import HtmlKakaku, HtmlAmazon, HtmlRakuten, HtmlMercari

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger('dom_log')
logger.setLevel(logging.INFO)
hdlr = logging.FileHandler(datetime.now().strftime('../log/dom_log_%Y_%m_%d.log'))
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)

class TestHtml(object):

    def __init__(self, class_name, *args, **kwargs):
        self.class_name = class_name

    def check_dom_change(self):
        target = re.sub('Html', '', self.class_name)
        logger.info('Start check %s DOM change' % target)

        html_class = eval(self.class_name)(settings, settings.keyword_test)
        try:
            html_class.build()

            products = html_class.products
            if len(products) < 1:
                SendMail().run(name)
        except Exception as ex:
            logger.error("Exception: %s" % ex, exc_info=True)

        logger.info('Finish check %s DOM change \n' % target)

if __name__ == '__main__':
    test_amazon = TestHtml('HtmlAmazon')
    test_amazon.check_dom_change()

    test_kakaku = TestHtml('HtmlKakaku')
    test_kakaku.check_dom_change()

    test_rakuten = TestHtml('HtmlRakuten')
    test_rakuten.check_dom_change()

    test_mercari = TestHtml('HtmlMercari')
    test_mercari.check_dom_change()
