# -*- coding: utf-8 -*-

import logging
from lib import utils

log = logging.getLogger(__name__)

class Product(object):
    def __init__(self, **kwargs):
        """The **kwargs argument may be filled with config values, which
        is added into the config object
        """
        self.html = u''
        self.doc = None
        self.name = u''
        self.price = u''
        self.type = u''
        self.manufacture = u''
        self.delivery_date_min = None
        self.delivery_date_max = None
        self.thumbnail = u''
        self.availability = 0
        self.total = None

    def get_json(self):
        if self.total is not None and self.total:
            self.total = int(self.total)
        else:
            self.total = None

        product_json = \
        {
            'name' : self.name,
            'url' : self.url,
            'price' : self.price,
            'type' : self.type,
            'manufacture' : self.manufacture,
            'delivery_date_min' : self.delivery_date_min,
            'delivery_date_max' : self.delivery_date_max,
            'thumbnail' : self.thumbnail,
            'availability' : self.availability,
            'total' : self.total,
        }

        return product_json

