# -*- coding: utf-8 -*-

import re
import traceback

from HTMLParser import HTMLParser
from api_scraping import ApiAmazon, ApiRakuten
from html_scraping import HtmlKakaku, HtmlAmazon, HtmlRakuten, HtmlMercari
from lib import network, utils

class Suggest(object):

    TYPE_AMAZON_API = 'webservices.amazon.co.jp'
    TYPE_AMAZON_HTML = 'amazon.co.jp'
    TYPE_KAKAKU = 'kakaku.com'
    TYPE_RAKUTEN_API = 'app.rakuten.co.jp'
    TYPE_RAKUTEN_HTML = 'search.rakuten.co.jp'
    TYPE_MERCARI = 'mercari.com'
    TYPE_MERCARI_ITEM = 'item.mercari.com'

    def __init__(self, app, settings, product_name, page = 1, ** kwargs):

        self.app = app
        self.settings = settings
        self.product_name = product_name
        self.filtered_product_name = u''
        self.page = page

        self.urls = []
        self.detail_urls = []

        self.products = []
        self.amazon_products = []
        self.kakaku_products = []
        self.rakuten_products = []
        self.mercari_products = []

        self.amazon_html_scraping_flg = False
        self.rakuten_html_scraping_flg = False

        self.amazon_suggest_flg = False

        self.api_amazon = ApiAmazon(self.settings)
        self.html_amazon = HtmlAmazon(self.settings)
        self.html_kakaku = HtmlKakaku(self.settings)
        self.api_rakuten = ApiRakuten(self.settings)
        self.html_rakuten = HtmlRakuten(self.settings)
        self.html_mercari = HtmlMercari(self.settings)

    def prepare(self):
        parser = HTMLParser()
        self.product_name = parser.unescape(self.product_name)
        self.product_name = re.sub(r'\s+', ' ', self.product_name)
        self.product_name = self.product_name.strip()

        self.api_amazon.prepare(self.product_name)
        self.urls.append(self.api_amazon.url)

        self.filtered_product_name = re.sub(r'\(.*\)', '', self.product_name)
        self.filtered_product_name = re.sub(r'\s+', ' ', self.filtered_product_name)

        self.html_kakaku.prepare(self.filtered_product_name)
        self.urls.append(self.html_kakaku.url)

        self.api_rakuten.prepare(self.filtered_product_name)
        self.urls.append(self.api_rakuten.url)

        self.html_mercari.prepare(self.filtered_product_name)
        self.urls.append(self.html_mercari.url)

    def prepare_recall(self):
        self.urls = []
        if self.amazon_html_scraping_flg:
            self.html_amazon.prepare(self.product_name)
            self.urls.append(self.html_amazon.url)

        if self.rakuten_html_scraping_flg:
            self.html_rakuten.prepare(self.product_name)
            self.urls.append(self.html_rakuten.url)

    def prepare_filter(self):
        # TODO: for future
        self.urls = []
        self.detail_urls = []
        pass

    def download(self):
        if len(self.urls) == 0:
            return

        requests = network.multithread_request(self.urls)

        for index, req in enumerate(requests):
            html = network.get_html(req.url, response = req.resp)
            type = utils.get_domain(req.url)
            self.parse(type, html)

    def download_detail(self):
        if len(self.detail_urls) == 0:
            return

        requests = network.multithread_request(self.detail_urls)

        for index, req in enumerate(requests):
            html = network.get_html(req.url, response = req.resp)

            if html and req.url in self.detail_urls:
                    self.detail_urls.remove(req.url)

            type = utils.get_domain(req.url)
            self.prepare_detail(type, html)

    def build(self):

        # process with full amazon product name
        self.prepare()
        self.download()

        self.prepare_recall()
        self.download()

        # if download fault, download one again
        for i in range(0, 2):
            self.download_detail()

        self.parse_detail()

        self.products = self.amazon_products[self.page - 1 : self.page] + self.kakaku_products[self.page - 1 : self.page] + \
                        self.rakuten_products[self.page - 1 : self.page] + self.mercari_products[self.page - 1 : self.page]

        self.products = sorted(self.products, key = lambda product: product.price)
        self.products = [product.get_json() for product in self.products]

    def parse(self, type, html):
        if not html:
            if type == self.TYPE_AMAZON_API:
                self.amazon_html_scraping_flg = True

            if type == self.TYPE_RAKUTEN_API:
                self.rakuten_html_scraping_flg = True

            return

        if type == self.TYPE_AMAZON_API:
            self.api_amazon.html = html
            self.api_amazon.parse()
            self.detail_urls = self.detail_urls + self.api_amazon.detail_urls

        if type == self.TYPE_AMAZON_HTML:
            self.html_amazon.html = html
            self.html_amazon.parse()
            self.amazon_products = self.html_amazon.products

        if type == self.TYPE_KAKAKU:
            self.html_kakaku.html = html
            self.html_kakaku.parse()
            self.kakaku_products = self.html_kakaku.products
            self.detail_urls = self.detail_urls + self.html_kakaku.detail_urls

        if type == self.TYPE_RAKUTEN_API:
            self.api_rakuten.html = html
            self.api_rakuten.parse()
            self.rakuten_products = self.api_rakuten.products

        if type == self.TYPE_RAKUTEN_HTML:
            self.html_rakuten.html = html
            self.html_rakuten.parse()
            self.rakuten_products = self.html_rakuten.products

        if type == self.TYPE_MERCARI:
            self.html_mercari.html = html
            self.html_mercari.parse()
            self.mercari_products = self.html_mercari.products

    def prepare_detail(self, type, html):
        if type == self.TYPE_AMAZON_API:
            self.api_amazon.detail_htmls.append(html)

        if type == self.TYPE_KAKAKU:
            self.html_kakaku.detail_htmls.append(html)

        if type == self.TYPE_MERCARI_ITEM:
            self.html_mercari.detail_htmls.append(html)

    def parse_detail(self):
        if len(self.api_amazon.detail_htmls) > 0:
            self.api_amazon.parse_detail()
            self.amazon_products = self.api_amazon.products

        if len(self.html_kakaku.detail_htmls) > 0:
            self.html_kakaku.parse_detail()
            self.kakaku_products = self.kakaku_products + self.html_kakaku.products
