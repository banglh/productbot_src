# -*- coding: utf-8 -*-
# Setting email
smpt_server = 'smtp.gmail.com'
smpt_port = 587
email_sender = 'DOM Monitor'
email_destination = 'thang@asilla.net'
email_user = 'asillatest01@gmail.com'
email_pass = 'Abc123z`'
email_type = 'plain'
email_charset = 'utf-8'

# Database infomation
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = '123456'
DB_NAME = 'alt-crawler'

# -*- coding: utf-8 -*-
request_timeout = 30
number_threads = 10
browser_user_agent = 'Mozilla/5.0 (compatible; U; ABrowse 0.6; Syllable) AppleWebKit/420+ (KHTML, like Gecko)'

amazon_search_url = 'https://www.amazon.co.jp/s/ref=nb_sb_noss?'
kakaku_search_url = 'http://kakaku.com/search_results/%s/?sort=popular&nameonly=off&lid=ksearch_searchbutton&l=l&act=Input&n=30'
rakuten_search_url = 'http://search.rakuten.co.jp/search/mall/%s/'
mercari_search_url = 'https://www.mercari.com/jp/search/?keyword=%s'

suggestion_api = 'http://localhost:5000/api/product/suggest?name=%s'

amazon_product_url = 'https://www.amazon.co.jp/dp/%s'
amazon_associate_tag = 'altsoho1134-22'

amazon_categories = { \
    u'DVD, DVD鑑賞、映画、ビデオ' : ['search-alias=dvd', 'Video'],
    u'Kindle, キンドル, 電子書籍、ebooks' : ['search-alias=digital-text', 'KindleStore'],
    u'ビデオ, 映画ダウンロード、オンライン映画' : ['search-alias=instant-video', 'VideoDownload'],
    u'デジタルミュージック, 音楽、音楽ダウンロード' : ['search-alias=digital-music', 'MP3Downloads'],
    u'Android, アンドロイド, アプリ' : ['search-alias=mobile-apps', 'MobileApps'],
    u'本, 雑誌、漫画、小説' : ['search-alias=stripbooks', 'Books'],
    u'洋書, 英文書' : ['search-alias=english-books', 'ForeignBooks'],
    u'ミュージック, 音楽' : ['search-alias=popular', 'Music'],
    u'クラシック, オーケストラ' : ['search-alias=classical', 'Classical'],
    u'TVゲーム, 家庭用ゲーム、ゲーム' : ['search-alias=videogames', 'VideoGames'],
    u'PCソフト, ソフトウェア、パソコンソフト' : ['search-alias=software', 'Software'],
    u'パソコン, 周辺機器, ハードディスク、パソコン機器' : ['search-alias=computers', 'PCHardware'],
    u'家電, カメラ, 掃除機、洗濯機、冷蔵庫、一眼レフ、デジタルカメラ' : ['search-alias=electronics', 'Electronics'],
    u'文房具, オフィス, 筆記用具' : ['search-alias=office-products', 'OfficeProducts'],
    u'ホーム, キッチン, キッチン用品、日用品' : ['search-alias=kitchen', 'Kitchen'],
    u'ペット, ペットフード、ペットの洋服' : ['search-alias=pets', 'PetSupplies'],
    u'ドラッグストア, 日用品、薬、医療品' : ['search-alias=hpc', 'HealthPersonalCare'],
    u'ビューティー, 化粧品、コスメ、美容' : ['search-alias=beauty', 'Beauty'],
    u'食品, 飲料, お酒, お菓子、食べ物、アルコール、ジュース' : ['search-alias=food-beverage', 'Grocery'],
    u'ベビー, マタニティ, 赤ちゃん用品、ママ用品' : ['search-alias=baby', 'Baby'],
    u'服, ファッション小物, 衣料品、おしゃれ' : ['search-alias=apparel', 'Apparel'],
    u'シューズ, バッグ, 靴、カバン' : ['search-alias=shoes', 'Shoes'],
    u'腕時計, 時計' : ['search-alias=watch', 'Watches'],
    u'ジュエリー, アクセサリー、宝石、ネックレス、指輪' : ['search-alias=jewelry', 'Jewelry'],
    u'おもちゃ, 玩具、人形' : ['search-alias=toys', 'Toys'],
    u'ホビー, 趣味' : ['search-alias=hobby', 'Hobbies'],
    u'楽器, 音楽' : ['search-alias=mi', 'MusicalInstruments'],
    u'スポーツ, アウトドア, スポーツ用品、アウトドア用品' : ['search-alias=sporting', 'SportingGoods'],
    u'カー, バイク, 車用品、バイクお手入れ' : ['search-alias=automotive', 'Automotive'],
    u'DIY, 工具, 機材、日曜大工' : ['search-alias=diy', 'HomeImprovement'],
    u'大型家電' : ['search-alias=appliances', 'Appliances'],
    u'クレジットカード' : ['search-alias=financial', 'CreditCards'],
    u'ギフト券, 優待券、割引券' : ['search-alias=gift-cards', 'GiftCards'],
    u'産業, 研究開発, 特殊機器、産業機器、研究開発機器' : ['search-alias=industrial', 'Industrial'],
    u'パントリー, 日用品の宅配、ネット通販' : ['search-alias=pantry', 'Blended'],
}

amazon_default_category = u'search-alias=aps'

rakuten_product_api = 'https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=json&keyword=%s&applicationId=%s&hits=10'
rakuten_app_ids = [ \
    '1019224554817943221',
    '1095408052929621527',
    '1055747376032940275',
    '1035857831628479397',
    '1083780446546557429',
    '1062341723314125274',
    '1065276010065740213',
    '1074007691682168222',
    '1045786426995604625',
    '1079874549277462146',
    '1018927112393397407',
    '1051994772534623223',
    '1069707876526181155',
    '1099794678727625399',
    '1030694621272279389',
    '1046228477860370473',
    '1003422134085687313',
    '1001591681454723190',
    '1064683190434688226',
    '1095540730061467317',
    '1026340945579821197',
]

aws_api_namespace = 'http://webservices.amazon.com/AWSECommerceService/2013-08-01'

aws_accounts = [ \
    ['AKIAJEVJPDUT4ZRDX64Q', 'Hm7yegmjy25U1QyDREZ+UmdPU7CddoI+FFxdiuqT', 'quan100-22'],
    ['AKIAJLH6B2T7QWEZALQQ', 'XrruRYq7xjhVHi3K5zYYhgSPAYPPmEEU+X+FCqrW', 'quan2-22'],
    ['AKIAIEYXIZZWH6R67QIQ', 'FMjW2SsDfnHQia5+I6h070RoiQAHNqf1e+0+1Q3t', 'quan3-22'],
    ['AKIAIS7RM3A5FUBLIUGA', '5imLQaj01lYL0gvfTLgQvvr11qPeIQw14RFgBnO9', 'quan4-22'],
    ['AKIAJAI6GGPA2B7L43VA', 'qaQwiZwts3gYZ7xFtzg5Vm9+bW6ypk6KWbxuoZaY', 'quan5-22'],
    ['AKIAJ2IAIUP27SDPOM6Q', 'Y+qZ71zxkiVWVZnbUGsxHmYXCZTNgs8UQsrXN55f', 'quan6-22'],
    ['AKIAJ7KEPMKEIADPBISA', '1EaoNv5iBOghtWG6bj5eb6pw0ACgCz2IH2ms83oi', 'quan7-22'],
    ['AKIAICSIV4LE7XEFHCLA', 'zx8UaozWcssS1/K/1USZBM8XunJTNtHA/LOtQ5SO', 'quan8-22'],
    ['AKIAIRDRJCYZAAICT53Q', 'JvdDvbOJVI2izZU+rA+tOj8xrlLX6fYU7nwxtwfB', 'quan9-22'],
    ['AKIAJ7ZYV65Z24QJD5XQ', 'x1alIQWQ+LzwG2wRUroAkiF0yQtwpONctyMLkRNj', 'quan10-22'],
    ['AKIAI3FGORUXNVLIFJRQ', 'IznMTICGxKDlmKqHJ2dSMj+CkcspDJaXKyw0ftuQ', 'quan11-22'],
    ['AKIAINCZHKCAWTST6B7A', 'VmJAmB4RMq4N4Hh8riItqNZrSO8KwmHHsIzO6q9j', 'quan12-22'],
    ['AKIAIW5L3K3JVMX425GA', 'B+0+qYc7RwXfF/2EytgD0X1bm4lv9h9CVyh53ZYo', 'quan13-22'],
    ['AKIAJHNDZ3VL4A5GP52Q', 'fVvcZnXtbxy6qFzx9VM0jy+qrd3bOVgBry8jjqyJ', 'quan14-22'],
    ['AKIAJWFRYD6VD6XD6D5Q', 'gSdM3BBdd5WNpFNC6uXECtBSwoao98PRkwnZX9aN', 'quan15-22'],
    ['AKIAINU2B7YUFOAEO3NQ', 'UD4o0J7D+duXIDDdxnuM/slp2whJWiJ8Z59HyM0w', 'quan16-22'],
    ['AKIAJ724CVCMPKRXCUEA', 'ze8EqUEqcfLvHu47oMu90x9mvXs1nXc+KPqzqlvg', 'quan17-22'],
    ['AKIAIHMNYAUANPVG7WIA', '9zG08T0lhsaYB4/IoJmcalEvYnWwByV+/TdXI4Dp', 'quan18-22'],
    ['AKIAIT2NSPOLZ5WMBAUA', 'KK6hhpG40qn/J7/xt5980txhEkAB1ULwXeq3pVKu', 'quan19-22'],
    ['AKIAIY2DZHHDZJBG4C5A', '4b8+mZHAO2CHXCSSUivc+T3O6NBdv735QF05wBt3', 'quan20-22'],
    ['AKIAJUULQUDX4NIWDNSA', 'YNjBECII3dL5JwxY7+xUM9BoFwA9vHa50AY4hgRE', 'quan21-22'],
    ['AKIAJQJSB2OOMZ6I2YPQ', '7QlSlpz/TxODWoLYG7zeGrNRImXL2qAtZYGEgutz', 'quan22-22'],
    ['AKIAJKCGEE4W4R2SNVJQ', 'S7u19txhx/F1Bgm76rDbRhLEoKacjIMqxx7tYmlt', 'quan23-22'],
    ['AKIAJ7EKXWKFAYP43T3Q', '01IqOT7m6HYrMyznfeMNBRJYfreZpv9lgYVqurg/', 'quan24-22'],
    ['AKIAIJYWKJQFZEBAYRLA', 'G9QSHfBTyuNFzB6EJ30Nq4/9OF4VKm0pDi9CvrZ2', 'quan25-22'],
    ['AKIAJNVVR6FOQLDYH3SQ', 'vOtMnDrUbdSf1CQhcKd58URdQYbdMW1gSEUDjAIc', 'quan26-22'],
    ['AKIAJTZH26SDYIWOTDGA', '2aaMW8ebtRK8+J7T15U41lI5ejwPH4kMQ2gvYmoF', 'quan27-22']
]

search_per_page = 5
product_per_page = 4
retry_time = 3
product_per_site = 10

# Keywords test for html change
keyword_test = u'macbook'