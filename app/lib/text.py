# -*- coding: utf-8 -*-
import os
import re
import string

TABSSPACE = re.compile(r'[\s\t]+', flags = re.UNICODE)

def innerTrim(value):
    if isinstance(value, (unicode, str)):
        # remove tab and white space
        value = re.sub(TABSSPACE, ' ', value)
        value = ''.join(value.splitlines())
        return value.strip()
    return ''
