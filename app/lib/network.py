# -*- coding: utf-8 -*-
import logging
import requests
import settings
import codecs

from mthreading import ThreadPool
from cookielib import CookieJar as cj

log = logging.getLogger()

def get_request_kwargs(timeout, useragent):
    """This Wrapper method exists b/c some values in req_kwargs dict
    are methods which need to be called every time we make a request
    """
    return {
        'headers': {'User-Agent': useragent},
        'cookies': cj(),
        'timeout': timeout,
        'allow_redirects': True
    }


def get_html(url, response = None):
    """Retrieves the html for either a url or a response object. All html
    extractions MUST come from this method due to some intricies in the
    requests module. To get the encoding, requests only uses the HTTP header
    encoding declaration requests.utils.get_encoding_from_headers() and reverts
    to ISO-8859-1 if it doesn't find one. This results in incorrect character
    encoding in a lot of cases.
    """
    FAIL_ENCODING = ['iso-8859-1', 'windows-31j']
    timeout = settings.request_timeout
    useragent = settings.browser_user_agent

    if response is not None:
        if response.status_code != 200:
            return u''

        if response.encoding is not None and response.encoding.lower() not in FAIL_ENCODING:
            return response.text
        return response.content

    try:
        html = None
        response = requests.get(url = url,
                                **get_request_kwargs(timeout, useragent))

        if response.status_code != 200:
            return u''

        if response.encoding is not None and response.encoding.lower() not in FAIL_ENCODING:
            html = response.text
        else:
            html = response.content

        if html is None:
            html = u''
        return html

    except Exception, e:
        log.error(e)


class MRequest(object):
    """Wrapper for request object for multithreading. If the domain we are
    crawling is under heavy load, the self.resp will be left as None.
    If this is the case, we still want to report the url which has failed
    so (perhaps) we can try again later.
    """
    def __init__(self, url):
        self.url = url

        self.timeout = settings.request_timeout
        self.useragent = settings.browser_user_agent
        self.resp = None

    def send(self):
        try:
            self.resp = requests.get(url = self.url, **get_request_kwargs(self.timeout, self.useragent))
        except Exception, e:
            log.error(e)


def multithread_request(urls):
    """Request multiple urls via mthreading, order of urls & requests is stable
    returns same requests but with response variables filled.
    """
    num_threads = settings.number_threads

    thread_pool = ThreadPool(num_threads)

    m_requests = []
    for url in urls:
        m_requests.append(MRequest(url))

    for req in m_requests:
        thread_pool.add_task(req.send)

    thread_pool.wait_completion()
    thread_pool.clear_threads()
    return m_requests
