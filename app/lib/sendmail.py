import smtplib
import settings

from datetime import datetime
from email.MIMEText import MIMEText

class SendMail(object):

    # const variable
    SUBJECT = " [Crawler] %s - Html structure maybe changed (%s)"
    BODY = """%s site's html structure maybe changed (%s). Crawler cannot get product information.\nPlease confirm it!"""

    # Initial parameters
    def __init__(self):
        # Initial connection
        self.conn = smtplib.SMTP(settings.smpt_server, settings.smpt_port)
        self.conn.ehlo()
        self.conn.starttls()
        self.conn.login(settings.email_user, settings.email_pass)

    # Send mail
    def run(self, name):
        subject = self.SUBJECT % (name, datetime.now().strftime('%Y-%m-%d'))
        body = self.BODY % (name, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        # Set mail header and body
        msg = MIMEText(body, settings.email_type, settings.email_charset)
        msg['Subject'] = subject

        msg['From'] = settings.email_sender
        msg['To'] = settings.email_destination

        # Try to send an email
        try:
            self.conn.sendmail(settings.email_sender, settings.email_destination, msg.as_string())
        except Exception, e:
            print "Send mail failed: %s" % str(e)
        finally:
            self.conn.close()
