# -*- coding: utf-8 -*-

import re
import urlparse
import urllib
import tldextract
import unicodedata
from bs4 import UnicodeDammit

def get_unicode(text, is_html = False):
    if not text:
        return u''

    if isinstance(text, unicode):
        return text

    converted = UnicodeDammit(text, is_html = is_html)

    if not converted.unicode_markup:
        raise Exception(
            'Failed to detect encoding of text: "%s"...,'
            '\ntried encodings: "%s"' %
            (text[:20], ', '.join(converted.tried_encodings)))

    return converted.unicode_markup

def get_price(text):
    if not text:
        return u''

    text = re.sub(ur'(¥|￥|円|,|\.)', u'', text)
    text = re.sub(ur'\s+', u'', text)

    return text

def get_real_url(url):
    parse_flg = True

    temp_url = url
    while parse_flg:
        parsed = urlparse.urlparse(temp_url)

        if not bool(parsed.scheme):
            break

        url = temp_url

        query_item = urlparse.parse_qs(parsed.query)
        if query_item.get('url'):
            temp_url = query_item['url'][0]
            continue

        if query_item.get('Url'):
            temp_url = query_item['Url'][0]
            continue

        if query_item.get('u'):
            temp_url = query_item['u'][0]
            continue

        if query_item.get('U'):
            temp_url = query_item['U'][0]
            continue

        if query_item.get('vc_url'):
            temp_url = query_item['vc_url'][0]
            continue

        if query_item.get('pc'):
            temp_url = query_item['pc'][0]
            continue

        parse_flg = False

    # remove query string
    url = urllib.unquote(url)
#     url = url[:url.find('?')]

    return url

def get_amazon_short_url(url, association_tag):
    s = re.search(r'\/(dp\/\w+)', url)

    if s is not None:
        url = 'https://www.amazon.co.jp' + s.group(0) + '/' + '?tag=' + association_tag

    return url

def is_second_hand_product(name):
    if re.search(ur'中古', name) is not None:
        return True

    return False

def get_delivery_date(delivery):
    if re.search(u'発送|出荷', delivery) is None:
        return []

    delivery = re.sub('\s+', '', delivery)
    search = re.search(ur'(\d+){0,}(～|〜|~)(\d+){0,}(.*)', delivery)

    if search is None:
        search = re.search(ur'(\d+)(～|〜|~){0}(\d+){0,}(.*)', delivery)

    if search is None:
        return []

    search_arr = search.groups()
    if not search_arr[0] and not search_arr[2]:
        return []

    min_date = None
    max_date = None

    if search_arr[0]:
        min_date = int(search_arr[0])

    if search_arr[0] and not search_arr[2]:
        min_date = None
        max_date = int(search_arr[0])

    if search_arr[2]:
        max_date = int(search_arr[2])

    if search_arr[3]:
        if u'時' in search_arr[3]:
            return []

        if u'週間' in search_arr[3]:
            if min_date:
                min_date = min_date * 7

            if max_date:
                max_date = max_date * 7

        if u'月' in search_arr[3]:
            if min_date:
                min_date = min_date * 30

            if max_date:
                max_date = max_date * 30

    return [min_date, max_date]

def get_domain(url):
    tld_dat = tldextract.extract(url)

    if tld_dat.subdomain:
        domain = tld_dat.subdomain + '.' + tld_dat.domain + '.' + tld_dat.suffix
    else:
        domain = tld_dat.domain + '.' + tld_dat.suffix

    domain = domain.replace('www.', '')

    return domain.lower()

def normalize_str(txt):
    txt = txt.strip()
#     txt = mb_convert_kana(txt, 'aKV', 'utf-8')
    txt = unicodedata.normalize('NFKC', txt)
    txt = txt.lower()
    return txt

def levenshtein_normalized_utf8(s1, s2, cost_ins = 1, cost_rep = 1, cost_del = 1):
    l1 = len(s1.decode('utf-8'))
    l2 = len(s2.decode('utf-8'))
    size = max(l1, l2)

    if size == 0:
        return 0

    if not s1:
        return l2 / size

    if not s2:
        return s1 / size

    return 1.0 - levenshtein_utf8(s1, s2, cost_ins, cost_rep, cost_del) / size

def levenshtein_utf8(s1, s2, cost_ins = 1, cost_rep = 1, cost_del = 1):
    s1 = list(s1)
    s2 = list(s2)
    l1 = len(s1)
    l2 = len(s2)

    if l1 == 0:
        return l2 * cost_ins

    if l2 == 0:
        return l1 * cost_del

    p1 = [0 for i in range(l2 + 1)]
    p2 = [0 for i in range(l2 + 1)]

    for i2 in range(l2 + 1):
        p1[i2] = i2 * cost_ins

    for i1 in range(l1):
        p2[0] = p1[0] + cost_ins
        for i2 in range(l2):
            c0 = p1[i2]
            if s1[i1] != s2[i2]:
                c0 = c0 + cost_rep

            c1 = p1[i2 + 1] + cost_del
            if c1 < c0:
                c0 = c1
            c2 = p2[i2] + cost_ins
            if c2 < c0:
                c0 = c2
            p2[i2 + 1] = c0

        tmp = p1
        p1 = p2
        p2 = tmp

    return p1[l2]

def unescape_quotes_json(text):
    pairs = text.split(',')
    pair_arrs = []
    for pair in pairs:
        pre, val = pair.split(':')
        parts = re.split('(")', val)
        n = parts.count('"')

        if n > 2:
            i = 1
            a = []
            for c in parts:
                if c == '"':
                    if 1 < i < n:
                        c = '\\"'
                    i += 1
                a.append(c)
            pair = pre + ':' + ''.join(a)

        pair_arrs.append(pair)

    return ','.join(pair_arrs)
