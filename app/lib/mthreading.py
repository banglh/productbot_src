# -*- coding: utf-8 -*-

import Queue
import traceback
import time
from threading import Thread

class Worker(Thread):
    """
    Thread executing tasks from a given tasks queue.
    """
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            try:
                func, args, kargs = self.tasks.get()
                if func is None:
                    break
            except Queue.Empty:
                traceback.print_exc()
                break
            try:
                func(*args, **kargs)
                time.sleep(0.2)
            except Exception:
                traceback.print_exc()

            self.tasks.task_done()


class ThreadPool:
    """
    Pool of threads consuming tasks from a queue.
    """
    def __init__(self, num_threads):
        self.tasks = Queue.Queue(num_threads)
        self.num_threads = num_threads
        for _ in range(num_threads):
            Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """
        Add a task to the queue.
        """
        self.tasks.put((func, args, kargs))

    def wait_completion(self):
        """
        Wait for completion of all the tasks in the queue.
        """
        self.tasks.join()

    def clear_threads(self):
        """
        """
        for i in range(self.num_threads):
            self.add_task(None)
