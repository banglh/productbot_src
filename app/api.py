# -*- coding: utf-8 -*-
#!flask/bin/python

import sys
import os
import re
import urllib
import settings
import requests
import requests_cache
import logging
import traceback
import ujson

from logging.handlers import RotatingFileHandler

from flask import Flask, jsonify, json, request, Response
from datetime import datetime

sys.path.append(os.path.realpath('..'))

from search import Search
from suggest import Suggest
from amazon_product import AmazonProduct
from bot import ProductBot
from lib import utils

app = Flask(__name__)

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler = RotatingFileHandler(datetime.now().strftime('log/error_log_%Y_%m_%d.log'))
handler.setFormatter(formatter)
app.logger.addHandler(handler)
app.logger.setLevel(logging.INFO)

ignored_parameters = ['Signature', 'AssociateTag', 'Timestamp', 'AWSAccessKeyId']
requests_cache.install_cache('alt_product_api_cache', backend = 'sqlite', expire_after = 86400, ignored_parameters = ignored_parameters)

@app.route('/api/product/search', methods = ['GET'])
def get_products():
    products = []

    keyword = request.args.get('keyword')
    page = request.args.get('page')

    if not keyword:
         return jsonify({'products': products})

    if not page:
        page = 1;
    else:
        page = int(page)

    try:
        search = Search(app, settings, keyword, page)
        search.build()
        products = search.products
    except Exception as e:
        app.logger.error(e)
        app.logger.error(str(traceback.format_exc()))

    return jsonify({'products': products})

@app.route('/api/product/suggest', methods = ['GET'])
def get_suggest():
    products = []

    product_name = request.args.get('name')
    page = request.args.get('page')

    if not product_name:
         return jsonify({'products': products})

    # paging
    if not page:
        page = 1;
    else:
        page = int(page)

    try:
        suggest = Suggest(app, settings, product_name, page)
        suggest.build()
        products = suggest.products
    except Exception as e:
        app.logger.error(e)
        app.logger.error(str(traceback.format_exc()))

    return jsonify({'products': products})

@app.route('/api/product', methods = ['GET'])
def get_amazon_product():
    product = {}
    asin = request.args.get('asin')

    if not asin:
         return jsonify(product)

    try:
        lookup = AmazonProduct(app, settings, asin)
        lookup.build()

        if lookup.product is not None:
            product = lookup.product

    except Exception as e:
        app.logger.error(e)
        app.logger.error(str(traceback.format_exc()))

    return jsonify({'product' : product})

@app.route('/api/bot/productbot', methods = ['POST'])
def get_product_bot():
    try:
        session_id = request.form.get('session_id').strip()
        message = request.form.get('message').strip()
        key_values = {}

        bot = ProductBot(app, settings, session_id, key_values)
        return Response(bot.response(message), mimetype = 'text/html')

    except Exception as e:
        app.logger.error(e)
        app.logger.error(str(traceback.format_exc()))

    return Response('{}', mimetype = 'text/html')

@app.route('/check', methods = ['GET', 'POST'])
def check():
    response = { \
        'status' : 1,
        'key_values' : {
            'product_num' : [],
            'shop_num' : []
        },
        'message' : 'ok',
        'message_html' : 'ok',
        'child_id' : 2,
    }
    return Response(ujson.dumps(response), mimetype = 'text/html')

if __name__ == '__main__':
    app.run(host = '0.0.0.0', debug = True)
