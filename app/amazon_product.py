# -*- coding: utf-8 -*-
import traceback

from api_scraping import ApiAmazonProduct
from html_scraping import HtmlAmazonProduct

class AmazonProduct(object):

    def __init__(self, app, settings, asin, ** kwargs):

        self.app = app
        self.settings = settings
        self.asin = asin
        self.product = None

    def build(self):
        try:
            lookup = ApiAmazonProduct(self.settings, self.asin)
            lookup.build()
            self.product = lookup.product

            if not lookup.html:
                lookup = HtmlAmazonProduct(self.settings, self.asin)
                lookup.build()
                self.product = lookup.product

        except Exception as e:
            self.app.logger.info('[Api Amazon Product] asin: ' + self.asin)
            self.app.logger.error(e)
            self.app.logger.error(str(traceback.format_exc()))
