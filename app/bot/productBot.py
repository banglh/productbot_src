# -*- coding: utf-8 -*-
from bot import Bot

import sys
import os
import re
import urllib
import traceback

from flask import json
from search import Search
from suggest import Suggest
from lib import utils
from ner import get_ner_crf

class ProductBot(Bot):

    STATE_ASK_PRODUCT_NAME = 'ask_product_name'
    STATE_ASK_PRODUCTS = 'ask_products'
    STATE_ASK_SHOPS = 'ask_shops'

    NUMBER_PREVIOUS = 8
    NUMBER_NEXT = 7
    NUMBER_SEARCH_AGAIN = 9

    ASK_PRODUCTNAME_CHILD_ID = 2
    ASK_PRODUCTS_CHILD_ID = None
    ASK_SHOPS_CHILD_ID = 6

    def __init__(self, app, settings, session_id, key_values):
        self.app = app
        self.settings = settings
        self.state = self.STATE_ASK_PRODUCT_NAME
        self.product_name = ''
        self.selected_product_index = None
        self.candidate_products = []
        self.shop_products = []
        self.search_page_index = 1

        super(ProductBot, self).__init__(session_id, key_values)

    def response(self, message):
        self.debug_hash['reponse_msg'] = message
        self.debug_hash['start_state'] = self.state

        if self.state == self.STATE_ASK_PRODUCT_NAME:
            self.extract_product_name(message)
            return self.answer_products()

        if self.state == self.STATE_ASK_PRODUCTS:
            return self.answer_shops(message)

        if self.state == self.STATE_ASK_SHOPS:
            return self.answer_detail(message)

        return self.answer_product_name()

    def answer_product_name(self):
        self.reset()
        self.set_state(self.STATE_ASK_PRODUCT_NAME)
        return self.response_json_prod(u'値段が知りたい商品名を入力して下さい', None, self.ASK_PRODUCTNAME_CHILD_ID)

    def extract_product_name(self, message):
        self.debug_hash['matchstart'] = 'match'
        self.debug_hash['matchtext'] = message
        self.product_name = message

        search = re.search(ur'(.+)の(最安値|値段|価格|料金|プライス)', message)
        matches = []

        if search is not None:
            matches = list(search.groups())
            self.product_name = matches[0]
        else:
            product_pattern_a = [ \
                ur'(.+)はいくら？',
                ur'(.+)はどう？',
                ur'(.+)はどれくらいかな？',
                ur'(.+)が知りたい',
                ur'(.+)が知りたいです',
                ur'(.+)でお願い',
                ur'(.+)がいい',
                ur'(.+)にして',
                ur'(.+)でお願いします',
                ur'(.+)で調べて',
                ur'(.+)で調べてくれますか',
                ur'(.+)はいくらくらい？',
            ]

            for pattern in product_pattern_a:
                search = re.search(pattern, message)
                if search is not None:
                    matches = list(search.groups())
                    self.product_name = matches[1]
                    break

        self.debug_hash['matches'] = matches

    def answer_products(self):
        datas, total = self.get_amazon_products(self.product_name, self.search_page_index)
        self.candidate_products = datas

        if len(datas) == 0:
            self.reset()
            res_message = u'残念ですが、お探しの商品がみつかりませんでした。\n値段が知りたい商品名を入力して下さい'
            res_message_html = u'残念ですが、お探しの商品がみつかりませんでした。\n値段が知りたい商品名を入力して下さい'
            return self.response_json_prod(res_message, res_message_html, self.ASK_PRODUCTNAME_CHILD_ID)

        self.set_state(self.STATE_ASK_PRODUCTS)
        self.save()

        pages = u''
        if total > 5:
            if self.search_page_index == 1:
                pages = u'7)次を見る\n'
            else:
                pages = u'8）前を見る\n'

        pages = pages + u'9）探しなおす'

        product_str = ''
        product_str_html = ''
        for key, data in enumerate(datas):
            num = key + 1
            product_str = product_str + str(num) + ')' + data['name'] + '\n\n'
            product_str_html = product_str_html + '<img src="' + data['thumbnail'] + '">\n' + str(num) + ')' + data['name'] + '\n\n'

        res_message = u'商品は下記にありますか？番号でお答えください。(page ' + str(self.search_page_index) + ')\n' + product_str + pages + '\n'
        res_message_html = u'商品は下記にありますか？番号でお答えください。(page ' + str(self.search_page_index) + ')\n' + product_str_html + pages + '\n'

        return self.response_json_prod(res_message, res_message_html, self.ASK_PRODUCTNAME_CHILD_ID)

    def answer_shops(self, message):
        index = None
        number = None

        command = None
        number = self.get_number(message)

        if self.selected_product_index is not None:
            index = self.selected_product_index

        elif number is not None and number > 0 and number <= 9:
            command_by_num = {self.NUMBER_NEXT : 'next', self.NUMBER_PREVIOUS : 'pre', self.NUMBER_SEARCH_AGAIN : 'back'};
            if number in command_by_num.keys():
                command = command_by_num[number]
            index = number - 1

        else:
            index = self.get_match_key(self.candidate_products, message)
            if index is None:
                # 次、前、探しなおす　をrmrで検索
                try:
                    result = urllib.urlopen('https://adg.alt.ai:80/api/rmr_single?api_key=8a087290f82631fcfcf8677bf0098efec1984224&question='
                                             + urllib.quote(message.encode('utf-8', 'ignore')))
                    result = json.loads(result.read())
                    if result:
                        answer = result[0]['answer']
                        if answer == '次':
                            command = 'next'
                        elif answer == '前':
                            command = 'pre'
                        elif answer == '探す':
                            command = 'back'
                except:
                    pass

        if command is not None:
            if command == 'next':
                self.search_page_index = self.search_page_index + 1
                self.save()
                return self.answer_products()

            elif command == 'pre':
                if self.search_page_index > 1:
                    self.search_page_index = self.search_page_index - 1
                    self.save()

                return self.answer_products()

            elif command == 'back':
                return self.answer_product_name()

        if index is None or index not in range(0, len(self.candidate_products)):
            return self.answer_products()

        selected_product = self.candidate_products[index]
        self.selected_product_index = index

        self.debug_hash['candidate_products'] = self.candidate_products
        products = self.get_all_shop_products(selected_product['name'])
        self.shop_products = products
        self.set_state(self.STATE_ASK_SHOPS)
        self.save()

        answer_str = u''
        answer_str_html = u''
        for key, product in enumerate(products):
            num = key + 1
            shop_name = self.get_shop_name_by_type(product['type'])
            # todo store,発送
            product['price'] = '{:0,.0f}'.format(product['price'])
            answer_str = answer_str + str(num) + ')' + product['name'] + '\n' + str(product['price']) + u'円 【' + \
                            shop_name + u'】\n'

            answer_str_html = answer_str_html + '<img src="' + product['thumbnail'] + '">\n' + str(num) + ')' + \
                            product['name'] + '\n' + str(product['price']) + u'円 【' + shop_name + u'】\n'

            if product['availability'] == 1 or product['delivery_date_max'] is not None:
                if product['total'] is not None:
                    answer_str = answer_str + str(product['total']) + u'在庫あり\n'
                    answer_str_html = answer_str_html + str(product['total']) + u'在庫あり\n'
                else:
                    answer_str = answer_str + u'在庫あり\n'
                    answer_str_html = answer_str_html + u'在庫あり\n'

            if product['delivery_date_max'] is not None:
                answer_str = answer_str + u'発送: ' + str(product['delivery_date_max']) + u'日以内\n'
                answer_str_html = answer_str_html + u'発送： ' + str(product['delivery_date_max']) + u'日以内\n'

            #answer_str = answer_str + product['url'] + '\n\n'
            answer_str = answer_str + '{link url="' + product['url'] + '" target="_blank"}' + product['url'] + '{/link}' + '\n\n'
            answer_str_html = answer_str_html + product['url'] + '\n\n'

        # TODO:maker
        self.reset()
        res_message = u'商品名：' + selected_product['name'] + u'\n最安順で並べてます。\n\n' + answer_str + \
                                       '\n\n' + u'他に知りたい商品はありますか？\n'
        res_message_html = u'商品名：' + selected_product['name'] + u'\n最安順で並べてます。\n\n' + answer_str_html + \
                                       '\n\n' + u'他に知りたい商品はありますか？\n'
        return self.response_json_prod(res_message, res_message_html, self.ASK_PRODUCTS_CHILD_ID)

    def answer_detail(self, message):
        index = None
        product = None

        if 'shop_num' in self.key_values and self.key_values['shop_num']:
            number = int(self.key_values['shop_num'][-1])
            if number == self.NUMBER_SEARCH_AGAIN:
                return self.answer_product_name()
            index = number - 1
        else:
            index = self.get_match_key(self.shop_products, message)

        if index is None or index not in range(0, len(self.shop_products)):
            return self.answer_shops(message)

        product = self.shop_products[index]
        self.reset()

        return self.response_json(u'かしこまりました。下記にて購入手続きを行って下さい。\n' + product['url'] + u'\n\n他に知りたい商品はありますか？\n')

    def response_json_prod(self, message, message_html = None, child_id = None):
        self.key_values['product_num'] = []
        self.key_values['shop_num'] = []
        self.save()

        return super(ProductBot, self).response_json(message, message_html, child_id)

    def reset(self):
        self.selected_product_index = None
        self.search_page_index = 1
        self.set_state(self.STATE_ASK_PRODUCT_NAME)
        self.key_values = {}
        self.save()

    def get_match_key(self, ary_a, needle, min_hit = 2):
        min_distance = sys.maxint
        index = None

        for key, t_product in enumerate(ary_a):
            product_str = ' '.join([data for data in t_product if data is not None])
            if 'type' in t_product:
                product_str = product_str + ' ' + ' '.join(self.get_shop_names_by_type(t_product['type']))

            product_str = utils.normalize_str(product_str)
            needle = utils.normalize_str(needle)
            distance = utils.levenshtein_utf8(product_str, needle, 1, 1, 0)


            if distance < min_distance and (len(needle) - distance) >= min_hit:
                index = key
                product = t_product
                min_distance = product

        return index

    def get_amazon_products(self, keyword, page = 1):
        try:
            search = Search(self.app, self.settings, keyword, page)
            search.build()

            return search.products, search.total

        except Exception as e:
            self.app.logger.error(e)
            self.app.logger.error(str(traceback.format_exc()))
            return [], 0

    def get_all_shop_products(self, name, page = 1):
        try:
            suggest = Suggest(self.app, self.settings, name, page)
            suggest.build()

            return suggest.products

        except Exception as e:
            self.app.logger.error(e)
            self.app.logger.error(str(traceback.format_exc()))
            return []

    def get_shop_name_by_type(self, type):
        type_a = { \
            'rakuten' : u'楽天',
            'amazon' : u'amazon',
            'kakaku' : u'価格com',
            'mercari' : u'メルカリ',
        }
        return type_a[type]

    def get_shop_names_by_type(self, type):
        type_a = { \
            'rakuten' : [u'楽天', u'らくてん'],
            'amazon' : [u'amazon', u'あまぞん'],
            'kakaku' : [u'価格com', u'かかくこむ'],
            'mercari' : [u'mercari', u'めるかり'],
        }
        return type_a[type]

    def get_number(self, message):
        ner = get_ner_crf(message)
        for data in ner:
            if data['entity'] == 'NUMBER':
                return int(data['surface'])
        return None

    def to_json(self):
        data = {
            'id' : self.id,
            'session_id' : self.session_id,
            'debug_hash' : self.debug_hash,
            'state' : self.state,
            'product_name' : self.product_name,
            'candidate_products' : self.candidate_products,
            'shop_products' : self.shop_products,
            'search_page_index' : self.search_page_index,
        }

        return json.dumps(data)

