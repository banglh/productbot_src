# -*- coding: utf-8 -*-

import urllib
import json
import re
import number_er

def get_ner_crf(query, raw = False):
    list = []
    number_er.extract(query, list, None)

    nes = []
    if raw:
        nes = {'results': list}
    else:
        for ne in list:
            if 'entity' in ne and ne['entity']:
                ne['entity'] = re.sub(r'^S-|^B-|^I-|^E-', '', ne['entity'])
                nes.append(ne)

    return nes
