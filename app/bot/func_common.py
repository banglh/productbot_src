# coding:utf-8
import re

def kansuji2arabic(text, list):
    """漢数字からアラビア数字への変換"""

    text = text[:]
    KANNUM_PATTERN_ONLY_KAN = re.compile(u'(?P<kansuji>[壱一二弐三参四五六七八九十拾百千万萬億兆]+)')
    matched = KANNUM_PATTERN_ONLY_KAN.search(text)
    if not matched:
        return text
    KANNUM_PATTERN = re.compile(u'(?P<kansuji>[壱一二弐三参四五六七八九十拾百千万萬億兆〇１２３４５６７８９０，,\d]+)')

    index = 0
    offset = 0
    while index < len(text):
        matched = KANNUM_PATTERN.search(text[index:])
        if matched:
            kansuji = matched.group('kansuji')
            startindex = matched.start('kansuji') + index
            endindex = matched.end('kansuji') + index
            result = 0
            kanindex = 0
            value_str = ''
            while kanindex < len(kansuji):
                c = kansuji[kanindex:kanindex + 1]
                kanindex += 1
                if c == u'〇０0':
                    value_str += '*10+'
                elif c in u'十拾':
                    value_str += '*10+'
                elif c == u'百':
                    value_str += '*100+'
                elif c == u'千':
                    value_str += '*1000+'
                elif c in u'万萬':
                    value_str += '*10000+'
                elif c in u'億':
                    value_str += '*' + str(10000 * 10000) + '+'
                elif c in u'兆':
                    value_str += '*' + str(10000 * 10000 * 10000) + '+'
                elif c in u'，,':
                    pass
                else:
                    if c in u'壱一１1':
                        value_str = value_str + '1'
                    elif c in u'二弐２2':
                        value_str = value_str + '2'
                    elif c in u'三参３3':
                        value_str = value_str + '3'
                    elif c in u'四４4':
                        value_str = value_str + '4'
                    elif c in u'五５5':
                        value_str = value_str + '5'
                    elif c in u'六６6':
                        value_str = value_str + '6'
                    elif c in u'七７7':
                        value_str = value_str + '7'
                    elif c in u'八８8':
                        value_str = value_str + '8'
                    elif c in u'九９9':
                        value_str = value_str + '9'
                    # digit *= 10

            if value_str.startswith('*'):
                value_str = value_str[1:]
            if value_str.endswith('+'):
                value_str = value_str[:-1]

            result = eval(value_str)
            # if result==0:
            #    result = digit
            offset -= len(str(result)) - len(text[startindex:endindex])
            list.append({'start':startindex + offset, 'len':kanindex, 'text':text[startindex:endindex], 'replace':result})
            text = u'%s%d%s' % (text[:startindex], result, text[endindex:])
            index = startindex + len('%d' % (result,))
        else:
            break
    return text
