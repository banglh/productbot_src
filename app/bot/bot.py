# -*- coding: utf-8 -*-
import time
import ujson
from phpserialize import serialize, unserialize
from flask import json, jsonify, request
from model import BotModel

class Bot(object):

    def __init__(self, session_id, key_values, state = None):
        self.id = None
        self.session_id = session_id
        self.key_values = key_values
        self.debug_hash = {}

        data = BotModel.get_server_bot(self.session_id)

        if data is not None and 'bot_serialized' in data and data['bot_serialized']:
            bot = unserialize(data['bot_serialized'])
            bot_hash = json.loads(bot)

            for key in bot_hash.keys():
                setattr(self, key, bot_hash[key])

        if state is not None:
            self.set_state(state)

        self.save()

    def set_state(self, state):
        self.state = state
        self.debug_hash['end_state'] = self.state
        self.save()

    def save(self):
        data = {}
        if self.id is None:
            data['session_id'] = self.session_id
            data['bot_serialized'] = ''
            data['updated'] = time.strftime('%Y-%m-%d %H:%M:%S')
            data = BotModel.insert(data)
            self.id = data['id']
        else:
            data['id'] = self.id
            data['session_id'] = self.session_id
            data['updated'] = time.strftime('%Y-%m-%d %H:%M:%S')

        data['bot_serialized'] = serialize(self.to_json())

        BotModel.update(data)

    def response_json(self, message, message_html = None, child_id = None):
        if request.environ['HTTP_HOST'] == 'dev_staging.alt.ai' \
                and re.search(r'test', request.environ['REQUEST_URI']) is not None:
            return '{}'


        hash = { \
            'status' : 1,
            'key_values' : self.key_values,
            'message' : message,
#             'debug' : self.debug_hash,
            'child_id' : child_id,
        }

        if message_html is not None:
            hash['message_html'] = message_html

        return ujson.dumps(hash)
