#! /usr/bin/python
# -*- coding: utf-8 -*-

import re, func_common, unicodedata

def extract(sentence, list, is_ner = False):

    replace_list = []
    sentence = func_common.kansuji2arabic(sentence, replace_list)
    sentence = unicodedata.normalize('NFKC', sentence)

    iterator = re.finditer("\d+", sentence)

    for match in iterator:
        list.append({'surface': match.group(), 'entity': 'NUMBER', 'prob': 1.0})
        if not is_ner:
            sentence = sentence.replace(match.group(), '')
        else:
            sentence = sentence.replace(match.group(), 'NUMBERNER')

    return sentence
