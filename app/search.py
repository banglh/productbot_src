# -*- coding: utf-8 -*-
import traceback

from HTMLParser import HTMLParser
from api_scraping import ApiSearch
from html_scraping import HtmlSearch

class Search(object):

    def __init__(self, app, settings, keyword, page = 1, ** kwargs):

        self.app = app
        self.settings = settings
        self.keyword = keyword
        self.page = page
        self.products = []
        self.total = 0

    def prepare(self):
        parser = HTMLParser()
        self.keyword = parser.unescape(self.keyword)
        self.keyword = self.keyword.strip()

    def build(self):
        self.prepare()

        search_flg = False
        try:
            search = ApiSearch(self.settings, self.keyword, self.page)
            search.build()
            self.products = search.products
            self.total = search.total

            if search.has_data:
                search_flg = True
        except Exception as e:
            self.app.logger.info('[Api Product Search] keyword: ' + self.keyword)
            self.app.logger.error(e)
            self.app.logger.error(str(traceback.format_exc()))

        # Call html scraping api
        if not search_flg:
            try:
                search = HtmlSearch(self.settings, self.keyword, self.page)
                search.build()
                self.products = search.products
                self.total = search.total
            except Exception as e:
                self.app.logger.info('[Html Product Search] keyword: ' + self.keyword)
                self.app.logger.error(e)
                self.app.logger.error(str(traceback.format_exc()))
