import os
import sys
import MySQLdb

sys.path.append(os.path.realpath('..'))
import settings

class Model(object):

    # Initial parameters
    def __init__(self):
        self.conn = None

    # Create connection
    def mysql_connect(self):
        self.mysql_close()
        self.conn = MySQLdb.connect(
            host = settings.DB_HOST,
            user = settings.DB_USER,
            passwd = settings.DB_PASS,
            db = settings.DB_NAME,
            charset = 'utf8',
            use_unicode = True
        )

    # Close connect
    def mysql_close(self):
        if self.conn != None:
            self.conn.commit()
            self.conn.close()
            self.conn = None

    # Excute query and bind param
    def execute_query(self, query, params = {}):
        # Execute query
        self.mysql_connect()
        self.conn.autocommit(False)
        cursor = self.conn.cursor()
        cursor.execute(query, params)

        insert_id = cursor.lastrowid
        # Close connection
        self.mysql_close()

        return insert_id

    # Excute many query
    def execute_many_query(self, queries, params = {}):
        # Execute query
        self.mysql_connect()
        self.conn.autocommit(False)
        cursor = self.conn.cursor()
        for query in queries:
            cursor.execute(query, params)

        # Close connection
        self.mysql_close()

    def fetchone_data(self, query, params = {}):
        # Execute query
        self.mysql_connect()
        cursor = self.conn.cursor()
        cursor.execute(query, params)

        # Get only one row
        result = cursor.fetchone()

        # Close connection
        self.mysql_close()
        return result

    def fetchall_data(self, query, params = {}):
        # Execute query
        self.mysql_connect()
        cursor = self.conn.cursor()
        cursor.execute(query, params)

        # Get multi rows
        result = cursor.fetchall()

        # Close connection
        self.mysql_close()
        return result

    # Delete connection
    def __del__(self):
        self.mysql_close()
