# -*- coding: utf-8 -*-
from model import Model

class BotModel():
    @staticmethod
    def get_server_bot(session_id):
        model = Model()

        query = 'SELECT id, session_id, bot_serialized, updated FROM server_bots WHERE session_id = %(session_id)s'
        params = {'session_id' : session_id}

        result = model.fetchone_data(query, params)

        data = {}
        if result is not None:
            data['id'] = result[0]
            data['session_id'] = result[1]
            data['bot_serialized'] = result[2]
            data['updated'] = result[3]

        return data

    @staticmethod
    def insert(data):
        model = Model()

        query = 'INSERT INTO server_bots(session_id, bot_serialized, updated) VALUES (%(session_id)s, %(bot_serialized)s, %(updated)s)'
        params = {'session_id' : data['session_id'], 'bot_serialized' : data['bot_serialized'], 'updated' : data['updated']}

        id = model.execute_query(query, params)
        data['id'] = id

        return data

    @staticmethod
    def update(data):
        model = Model()

        query = 'UPDATE server_bots SET session_id = %(session_id)s, bot_serialized = %(bot_serialized)s, \
            updated = %(updated)s WHERE id = %(id)s'
        params = { \
                'id' : data['id'],
                'session_id' : data['session_id'],
                'bot_serialized' : data['bot_serialized'],
                'updated' : data['updated'],
        }

        model.execute_query(query, params)
